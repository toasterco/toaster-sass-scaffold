#Toaster Sass Scaffold
---

Basic SASS folder structure and set of mixins/functions to be used as a submodule in a web project.  
The project uses a few libraries:

- [normalize.css](http://necolas.github.io/normalize.css/): consisency between browsers.


The entrypoint is `critial.scss`. You can either use it as your main sass file (suggested), or rename it with a leading underscore and use it as a partial.


##Requirements
---

- Latest version of Sass (`sudo gem install sass` or `sudo gem update sass` if already installed)
- Perform `bower install` in the root folder to install the latest versions of normalize.css


##TODOs
---

- Add mixins (vertical centering / fixed ratio element using padding bottom)
- Add example uses (e.g. using the retina mixin on an example spritesheet)
- Consider using other libs from Thoughtbot (bitters)